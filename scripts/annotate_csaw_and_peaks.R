library(ChIPseeker)
library(GenomicRanges)
library(TxDb.Hsapiens.UCSC.hg38.knownGene)
library(GenomicOperations)

# Retrieve all csaw and peak calling result files.
csaw_results = Sys.glob("output/csaw/*.txt")
narrow_peak_results = Sys.glob("output/pipeline/peak_call/*/*.narrowPeak")

# Loop over results, loading and annotating them.
for(i in c(csaw_results, narrow_peak_results)) {
    ext = tools::file_ext(i)
    if(ext=="txt") {
        peaks = GRanges(read.table(i, sep="\t", header=TRUE))
    } else {
        peaks = rtracklayer::import(i, format=tools::file_ext(i))
    }
    
    if(length(peaks) > 0) {
        annot = annotatePeak(peaks, tssRegion=c(-1000, 1000), 
                             TxDb=TxDb.Hsapiens.UCSC.hg38.knownGene,
                             annoDb="org.Hs.eg.db")
        write.table(as.data.frame(annot), file=paste0(i, ".annotated"),
                    sep="\t", col.names=TRUE, row.names=FALSE)
    }
}

# Build vectors representing the peak call's characteristics.
names(narrow_peak_results) = gsub(".*peak_call\\/(.*)\\/.*peaks.narrowPeak", "\\1", narrow_peak_results)
all_peaks = lapply(narrow_peak_results, rtracklayer::import, format="narrowPeak")
chip_targets = gsub("(.*)_(.*)_1", "\\1", names(all_peaks))
timepoint = gsub("(.*)_(.*)_1", "\\2", names(all_peaks))
all_peaks = lapply(all_peaks, function(x) { x[grepl("^chr(\\d+|MT|X|Y)$", seqnames(x))] })

# Also read other MCF7 ChIP results.
mcf7_peaks = Sys.glob("output/MCF7_pipeline/peak_call/*/*.narrowPeak")
names(mcf7_peaks) = gsub(".*peak_call\\/MCF7_(.*)\\/.*peaks.narrowPeak", "\\1", mcf7_peaks)
mcf7_peaks = lapply(mcf7_peaks, rtracklayer::import, format="narrowPeak")

# For each target protein (HSF1, etc), generate an overlap
# and annotate each region with the time-points it was found in. 
for(chip_target in unique(chip_targets)) {
    peak_subset = GRangesList(all_peaks[chip_target == chip_targets])
    go = GenomicOverlaps(peak_subset)
    
    annot = annotatePeak(combined_regions(go), tssRegion=c(-1000, 1000), 
                         TxDb=TxDb.Hsapiens.UCSC.hg38.knownGene,
                         annoDb="org.Hs.eg.db")
                         
    factor_mat = do.call(cbind, lapply(mcf7_peaks, function(x) { countOverlaps(combined_regions(go), x) }))
    
    filename = paste0("output/", chip_target, " annotated combined regions.txt")                      
    write.table(cbind(as.data.frame(annot), intersect_matrix(go), factor_mat),
                file=filename, sep="\t", col.names=TRUE, row.names=FALSE)
}
